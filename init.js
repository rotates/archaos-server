var http = require('http'),
	net = require('net'),
	msgpack = require('msgpack-js'),
	crypto = require('crypto'),
	Data = require("./libs/data.js"),
	Unit = require("./libs/unit.js");
	Utils = require("./libs/utils.js"),
	Server = require("./libs/server.js"),
	Game = require("./libs/game.js"),
	msgpack = require('msgpack-js'),
	fs = require('fs'),
	util = require('util'),
	zlib = require('zlib');

var options = {
		connection: {
			http: {
				ip: "82.4.14.208",
				port:3000
			},
			tcp: {
				ip: "82.4.14.208",
				port: 3001
			}
		}
	};


// --- HTTP server

var app = require('express')();
app.use(require('express').compress());
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
app.enable("jsonp callback");

app.get('/data/:type', function (req, res) {
	var output = null;
	switch (req.params.type) {
		case "units":
			output = Unit.getUnitDefs();
		break;
	}
	if (output !== null) {
		res.json(output);
		Utils.log("Sending '" + req.params.type + ".json' to '" + req.host + "'", "data");
	}
	else {
		res.json({
			response: Utils.error(49),
			currentTime: new Date().getTime()
		});
	}
});

app.get('/server/:action?', function (req, res) {
	var output, query;
	if (req.params.action) {
		query = req.query || {};
		query.action = req.params.action;
		Server.process(query, function(output) {
			if (output) {
				res.json({
					response: output,
					currentTime: new Date().getTime()
				});
			}
			else {
				res.json({
					response: Utils.error(22),
					currentTime: new Date().getTime()
				});
			}
		});
	}
});

app.get('/games/:game?/:action?', function (req, res) {
	var output, player, query, action;
	if (req.params.game) {
		action = req.params.action || "refresh";
		query = req.query ? req.query : {};
		query.action = action;

		query.game = req.params.game;

		Game.process(query, function(output) {
			if (output) {
				res.json({
					response: output,
					currentTime: new Date().getTime()
				});
			}
		});
	}
	else {
	    Data.getGames(req.query.username, req.query.limit, function (games) {
			res.json({
				response: games,
				currentTime: new Date().getTime()
			});
		});
	}

});

app.get('/crossdomain.xml', function (req, res) {
	res.set('Content-Type', 'text/xml');
	res.send("<?xml version=\"1.0\"?>\n" +
		"<!DOCTYPE cross-domain-policy SYSTEM \"http://www.adobe.com/xml/dtds/cross-domain-policy.dtd\">\n" +
		"<cross-domain-policy>\n" +
		"<allow-http-request-headers-from domain=\"*\" headers=\"*\" />\n" +
		"<site-control permitted-cross-domain-policies=\"all\"/>\n" +
		"<allow-access-from domain=\"*\" to-ports=\"" + options.connection.http.port + "," + options.connection.tcp.port + "\"/>\n" +
		"</cross-domain-policy>\n");
	Utils.log("Policy file sent (HTTP): " + req.host, "verbose");
});

app.listen(options.connection.http.port);

// --- TCP Server

var policyserver = net.createServer(function(socket) {
	socket.setTimeout(3000);
	socket.on('data', function(data) {
		if (data == "<policy-file-request/>\0") {
			socket.setEncoding('utf8');
			socket.write("<?xml version=\"1.0\"?>");
			socket.write("<!DOCTYPE cross-domain-policy SYSTEM \"http://www.adobe.com/xml/dtds/cross-domain-policy.dtd\">\n");
			socket.write("<cross-domain-policy>\n");
			socket.write("<allow-http-request-headers-from domain=\"*\" headers=\"*\" secure=\"false\" />\n");
			socket.write("<site-control permitted-cross-domain-policies=\"all\"/>\n");
			socket.write("<allow-access-from domain=\"*\" to-ports=\"" + options.connection.http.port + "," + options.connection.tcp.port + "\" secure=\"false\"/>\n");
			socket.write("</cross-domain-policy>\n\0");
			socket.end();
			Utils.log("Master policy file sent (TCP): " + socket.remoteAddress, "verbose");
			return;
		}
	});
}).listen(843);

var socketserver = net.createServer(function(socket) {
	var index,
		s = {
			socket: socket,
			remoteAddress: socket.remoteAddress,
			player: null,
			write: function(data) {
				this.socket.write(JSON.stringify({
					response: data,
					currentTime: new Date().getTime()
				}) + "\0");
			}
		};
	s.socket.setKeepAlive(true, 10000);

	s.socket.on('connect', function() {
		Utils.log("Connection opened with " + s.remoteAddress, "verbose");
	});

	s.socket.on('data', function(data) {
		var req, res, output;
		if (data == "<policy-file-request/>\0") {
			s.socket.setEncoding('utf8');
			s.socket.write("<?xml version=\"1.0\"?>");
			s.socket.write("<!DOCTYPE cross-domain-policy SYSTEM \"http://www.adobe.com/xml/dtds/cross-domain-policy.dtd\">\n");
			s.socket.write("<cross-domain-policy>\n");
			s.socket.write("<allow-http-request-headers-from domain=\"*\" headers=\"*\" secure=\"false\" />\n");
			s.socket.write("<site-control permitted-cross-domain-policies=\"all\"/>\n");
			s.socket.write("<allow-access-from domain=\"*\" to-ports=\"" + options.connection.http.port + "," + options.connection.tcp.port + "\" secure=\"false\"/>\n");
			s.socket.write("</cross-domain-policy>\n\0");
			s.socket.end();
			Utils.log("Policy file sent (TCP): " + s.remoteAddress, "verbose");
			return;
		}
		else {
			try {
				req = msgpack.decode(data);
			}
			catch (e) {
				return;
			}
		}

		if (req.hasOwnProperty("sid") && req.hasOwnProperty("sys")) {
			switch (req.sys) {
				case "data": {
					if (req.hasOwnProperty("type")) {
						var file, stat, readStatic;
						switch (req.type) {
							case "units":
								output = Unit.getUnitDefs();
							break;
						}
						if (output) {
							output.sid = req.sid;
							Utils.log("Sending '" + req.type + ".json' to '" + s.remoteAddress + "'", "data");
							s.write(output);
						}
					}
				}
				break;
				case "server":
					if (req.hasOwnProperty("action")) {
						Server.process(req, function(output){
							if ((req.action == "ident" || req.action == "login") && output.hasOwnProperty("success")) {
								if (!Data.properties.connections.hasOwnProperty(req.username)) {
									Data.properties.connections[req.username] = [];
								}
								Data.properties.connections[req.username].push(s.socket);
								s.player = req.username;
								Utils.log("Player '" + s.player + "' bound to socket", "verbose");
							}
							if (output) {
								output.sid = req.sid;
								s.write(output);
							}
						});
					}
					else {
						output = Utils.error(22);
						output.sid = req.sid;
						s.write(output);
					}
				break;
				case "games":
					if (req.hasOwnProperty("game")) {
						req.action = req.action || "refresh";
						Game.process(req, function(output) {
							output.sid = req.sid;
							s.write(output);
						});
					}
					else {
						Data.getGames(req.username, req.limit, function(output) {
							output.sid = req.sid;
							s.write(output);
						});
					}
				break;
			}
		}
	});

	s.socket.on('close', function() {
		Utils.log("Connection closed with " + s.remoteAddress, "verbose");
		if (s.player !== null && Data.properties.connections.hasOwnProperty(s.player)) {
			Data.properties.connections[s.player].splice(Data.properties.connections[s.player].indexOf(s.socket),1);
			Utils.log("Player '" + s.player + "' unbound from socket", "verbose");
		}
	});

	s.socket.on('timeout', function() {
		s.socket.end();
	});

}).listen(options.connection.tcp.port);

Utils.log('Server started. Waiting for connections at http://' + options.connection.http.ip + ':' + options.connection.http.port + '/');
