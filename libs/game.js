var Data = require("./data.js"),
	Utils = require("./utils.js"),
	Board = require('./board.js'),
	Point = require('./point.js'),
	Unit = require("./unit.js"),
	msgpack = require("msgpack-js"),
	_ = require("cloneextend");

// --- Dice

// massive thanks to @andy_herbert for this pseudo-random number generator

var Dice = (function () {
	function random(game) {
		game.seed = (214013 * game.seed + 2531011) & 0x7fffffff;
		return game.seed / 0x7fffffff;
	}
	function roll(game, sides) { // returns a number 0 - sides;
		return Math.floor(random(game) * ((sides === undefined) ? 10 : sides));
	}
	function rollAgainst(game, atk, def) {
		var atkRoll, defRoll;
		atkRoll = Dice.roll(game);
		defRoll = Dice.roll(game);
		return (atk + atkRoll >= def + defRoll);
	}
	return {
		"random": random,
		"roll": roll,
		"rollAgainst": rollAgainst
	};
})();

exports.killUnit = function (game, unit) {
	if (Unit.hasStatus(unit, "wizard")) {
		exports.doDefeat(game, game.players(unit.owner));
		return;
	}
	if (Unit.hasStatus(unit, "undead") || Unit.hasStatus(unit, "spread") || unit.illusion) {
		delete game.units[unit.id];
		exports.newAction(game, "removedUnits", {
			units: [unit.id]
		});
	}
	else {
		unit.dead = true;
		exports.newAction(game, "killed", {
			unit: unit.id
		});
	}
	if (unit.occupier) {
		unit.occupier.position = unit.position;
		exports.newAction(game, "dismounted", {
			unit: unit.occupier.id,
			from: unit.position,
			to: unit.position
		});
	}
};

exports.newTurn = function (game) {
	var lastAction, gameUnit;

	if (!exports.checkVictory(game)) {
		var defeated = true;
		while (defeated) {
			game.currentPlayer++;
			if (game.currentPlayer > game.players.length - 1) {
				game.currentPlayer = 0;
				/*
				game.phase++;
				if (game.phase > 4) {
					game.phase = 1;
					game.round++;
				}
				*/
				// for debugging, stick to movement phase only
				game.phase = 3;
				game.round++;
			}
			if (!game.players[game.currentPlayer].defeated) {
				defeated = false;
			}
		}

		var adjacentUnits, engagedArray;

		for (gameUnit in game.units) {
			game.units[gameUnit].moved = false;
			game.units[gameUnit].attacked = false;
			game.units[gameUnit].rangedAttacked = false;
			game.units[gameUnit].engaged = false;
		}

		lastAction = exports.newAction(game, "newTurn", {
			round: game.round,
			phase: game.phase,
			currentPlayer: game.currentPlayer
		});
		for (gameUnit in game.units) {
			if (game.units[gameUnit].owner == game.currentPlayer) {
				adjacentUnits = Board.getAdjacent(game, game.units[gameUnit]);
				engagedArray = [];
				if (adjacentUnits.length > 0) {
					for (var i = 0; i < adjacentUnits.length; i++) {
						if (adjacentUnits[i].id != game.units[gameUnit].id && adjacentUnits[i].properties.mnv > 0 && Dice.rollAgainst(game, adjacentUnits[i].properties.mnv, game.units[gameUnit].properties.mnv)) {
							game.units[gameUnit].engaged = true;
							engagedArray.push({
								id: game.units[gameUnit].id,
								to: adjacentUnits[i].id
							});
						}
					}
					if (engagedArray.length > 0) {
						exports.newAction(game, "engaged", {
							units: engagedArray
						});
					}
				}
			}
		}
	}
	Data.save("games", game);
	return lastAction;
};

exports.newAction = function (game, type, data) {
	var s, sockets, p, action, remoteAction;

	// first add the action to the game and store in the database
	action = {
		id: game.actions.length + 1,
		type: type,
		data: _.clone(data),
		time: new Date().getTime()
	};
	game.actions.push(action);
	game.lastAction = game.actions.length;
	game.modified = new Date().getTime();
	Data.save("games", game);

	// then send the action to connected players

	remoteAction = JSON.parse(JSON.stringify(action));
	remoteAction.game = game.id;
	// remoteAction = msgpack.encode(remoteAction);
	remoteAction = JSON.stringify(remoteAction);
	for (p in game.players) {
		if (Data.properties.connections.hasOwnProperty(p.username) && Data.properties.connections[p.username].length > 0) {
			sockets = Data.properties.connections[p.username];
			for (s in player) {
				if (s.writeable) {
					s.write(remoteAction + "\0");
				}
			}
			Utils.log("Action '" + action.type + "' in game '" + game.id + "' sent to '" + p.username + "'", "debug");
		}
	}
	return game.lastAction;
};

exports.addPlayer = function (game, user) {
	game.players.push({
		username: user.username,
		handle: user.handle,
		defeated: false
	});
	Data.save("games", game);
};

exports.doDefeat = function (game, player) {
	var user, lastAction;
	player = Utils.find(game.players, "username", player.username);
	if (player !== null) {
		player.defeated = true;
		Data.getUser(player.username, function(user) {
			user.timesLost++;
			Data.save("users", user);
		});

		exports.newAction(game, "defeated", {
			username: player.username
		});

		var removedUnits = [];
		for (var u in game.units) {
			if (game.units[u].owner == playerIndex && game.units[u].dead === false) {
				removedUnits.push(u);
				delete game.units[u];
			}
		}
		lastAction = exports.newAction(game, "removedUnits", {
			units: removedUnits
		});

		exports.checkVictory(game);

		if (game.currentPlayer == playerIndex) {
			exports.newTurn(game);
		}

		return lastAction;
	}
};

exports.checkVictory = function (game) {
	var undefeated = game.players.filter(function (el) {
		return (el.defeated === false);
	});

	if (undefeated.length < 2) {
		game.ended = new Date().getTime();
		game.modified = new Date().getTime();
		if (undefeated.length == 1) {
			lastAction = exports.newAction(game, "ended", {
				reason: "A player has won the game",
				player: undefeated[0].username
			});
			Data.getUser(undefeated[0].username, function(user) {
				user.timesWon++;
				Data.save("users", user);
			});

		}
		else {
			lastAction = exports.newAction(game, "ended", {
				reason: "All players defeated"
			});
		}
		return true;
	}
	else {
		return false;
	}
};

exports.process = function (atts, callback) {
	var player, unit, actionId, from, to, success, targetUnit, undefeated, targetLoc, dist, path, steps, output;

	if (!atts.username) {
		callback.call(null, Utils.error(2));
		return;
	} // no player specified
	if (!atts.game) {
		callback.call(null, Utils.error(1));
		return;
	} // no game specified
	Data.getGame(atts.game, function(game) {
		if (!game) {
			callback.call(null, Utils.error(4));
			return;
		} // game does not exist
		Data.getUser(atts.username, function(user) {
			if (!user) {
				callback.call(null, Utils.error(5));
				return;
			} // player does not exist
			if (user.token === 0) {
				callback.call(null, Utils.error(27));
				return;
			} // not logged in
			if (!atts.token || atts.token != user.token) {
				callback.call(null, Utils.error(45));
				return;
			} // invalid or missing access token
			player = Utils.find(game.players, "username", user.username);
			switch (atts.action) {
				// - send a message in a game
				case "message":
					if (!player) {
						callback.call(null, Utils.error(13));
						return;
					} // player is not in this game
					if (!atts.text) {
						callback.call(null, Utils.error(29));
						return;
					} // no text specified
					if (atts.text.length > 255) {
						callback.call(null, Utils.error(30, {
							maxLength: 255
						}));
						return;
					} // message too long
					actionId = exports.newAction(game, "message", {
						username: user.username,
						text: atts.text
					});
					output = {
						success: "Player sent a message",
						data: {
							username: user.username,
							text: atts.text
						},
						actionId: actionId
					};
					callback.call(null, output);
					return;
				// - join a game
				case "joined":
					if (game.players.length >= game.maxPlayers) {
						callback.call(null, Utils.error(12, {
							maxPlayers: game.maxPlayers
						}));
						return;
					} // game is full
					if (game.active) {
						callback.call(null, Utils.error(10));
						return;
					} // game has already begun
					if (game.ended) {
						callback.call(null, Utils.error(23));
						return;
					} // game has ended
					if (player) {
						callback.call(null, Utils.error(9));
						return;
					} // player is already in this game
					exports.addPlayer(game, user);
					exports.newAction(game, "joined", {
						username: user.username,
						handle: user.handle
					});
					output = {
						success: "Player joined game",
						data: {
							username: user.username,
							game: game.id
						}
					};
					callback.call(null, output);
					return;
				// - leave a game
				case "left":
					if (!player) {
						callback.call(null, Utils.error(13));
						return;
					} // player is not in this game
					if (game.ended) {
						callback.call(null, Utils.error(23));
						return;
					} // game has ended
					if (game.active) {
						exports.doDefeat(game, player);
						if (game.players[game.currentPlayer].username === player.username) {
							exports.newTurn(game);
						}
						undefeated = game.players.filter(function (el) {
							return (el.defeated === true);
						});
						if (undefeated.length < 2) {
							game.ended = true;
							exports.newAction(game, "ended", {
								reason: "Insufficient players"
							});
						}
						output = {
							success: "Player forfeit game",
							data: {
								username: player.username,
								game: game.id
							}
						};
						callback.call(null, output);
						return;
					}
					else {
						exports.newAction(game, "left", {
							username: player.username
						});
						var playerIndex = game.players.indexOf(player);
						game.players.splice(playerIndex, 1);
						if (game.players.length < 1) {
							Data.deleteGame(game);
							Utils.log("Deleted game '" + atts.game + "' due to insufficient players", "warn");
						}
						else {
							if (playerIndex === 0) {
								exports.newAction(game, "newOwner", {
									username: game.players[0].username
								});
							}
						}
						output = {
							success: "Player left game",
							data: {
								username: atts.username,
								game: atts.game
							}
						};
						callback.call(null, output);
						return;
					}
					break;
				// - start a game
				case "gameStarted":
					if (!player) {
						callback.call(null, Utils.error(13));
						return;
					} // player is not in this game
					if (game.players.indexOf(player) !== 0) {
						callback.call(null, Utils.error(31, {
							owner: game.players[0].username
						}));
						return;
					} // player is not the game owner
					if (game.active) {
						callback.call(null, Utils.error(10));
						return;
					} // game has already begun
					if (game.ended) {
						callback.call(null, Utils.error(23));
						return;
					} // game has ended
					if (game.players.length < 2) {
						callback.call(null, Utils.error(11, {
							players: game.players.length
						}));
						return;
					} // need more players
					game.active = true;
					game.round = 1;
					game.phase = 1;
					game.currentPlayer = 0;
					Board.setupWizards(game);
					// TODO: better multi-user modify query
					for (var p in game.players) {
						if (!game.players[p].defeated) {
							Data.getUser(game.players[p].username, function(u) {
								u.timesPlayed++;
								Data.save("users", u);
							});
						}
					}
					exports.newAction(game, "gameStarted", {
						units: game.units,
						currentPlayer: game.currentPlayer
					});
					output = {
						success: "Game has begun",
						data: {
							units: game.units,
							currentPlayer: game.currentPlayer
						}
					};
					callback.call(null, output);
					return;
				// - end turn
				case "endturn":
					if (!game.active) {
						callback.call(null, Utils.error(20));
						return;
					} // game has not yet begun
					if (game.ended) {
						callback.call(null, Utils.error(23));
						return;
					} // game has ended
					if (!player) {
						callback.call(null, Utils.error(13));
						return;
					} // player is not in this game
					if (game.players[game.currentPlayer].username !== player.username) {
						callback.call(null, Utils.error(14));
						return;
					} // not this player's turn
					actionId = exports.newTurn(game);
					output = {
						success: "Turn ended",
						data: {
							currentPlayer: game.currentPlayer,
							round: game.round,
							phase: game.phase,
							actionId: actionId
						}
					};
					callback.call(null, output);
					return;
				// - move in a game
				case "moved":
					if (!game.active) {
						callback.call(null, Utils.error(20));
						return;
					} // game has not yet begun
					if (game.ended) {
						callback.call(null, Utils.error(23));
						return;
					} // game has ended
					if (!player) {
						callback.call(null, Utils.error(13));
						return;
					} // player is not in this game
					if (game.players[game.currentPlayer].username !== player.username) {
						callback.call(null, Utils.error(14));
						return;
					} // not this player's turn
					if (!atts.unit) {
						callback.call(null, Utils.error(3));
						return;
					} // unspecified unit name
					if (!game.units[atts.unit]) {
						callback.call(null, Utils.error(6));
						return;
					} // unit does not exist
					unit = game.units[atts.unit];
					if (unit.dead) {
						callback.call(null, Utils.error(35));
						return;
					} // unit is dead
					if (unit.owner !== game.players.indexOf(player)) {
						callback.call(null, Utils.error(19));
						return;
					} // player does not own this unit
					if (unit.moved === true) {
						callback.call(null, Utils.error(18, {
							unit: atts.unit
						}));
						return;
					} // unit has already moved
					if (unit.engaged === true) {
						callback.call(null, Utils.error(37, {
							unit: atts.unit
						}));
						return;
					} // unit is engaged
					if (unit.properties.mov === 0) {
						callback.call(null, Utils.error(36));
						return;
					} // unit cannot move
					if ((!atts.x && !atts.y) && !atts.path) {
						callback.call(null, Utils.error(15));
						return;
					} // unspecified move co-ords
					if (atts.path && Unit.hasStatus(unit, 'flying')) {
						callback.call(null, Utils.error(41));
						return;
					} // unit cannot move via a path
					if ((atts.x && atts.y) && !Unit.hasStatus(unit, 'flying')) {
						callback.call(null, Utils.error(42));
						return;
					} // unit cannot move via a co-ordinate
					if (atts.path) {
						path = atts.path.match(/(\d+-\d+)+/g);
						if (path.length < 1) {
							callback.call(null, Utils.error(40));
							return;
						}
						path = path.map(function (el) {
							var pt = el.split("-");
							return new Point(pt[0], pt[1]);
						});
						// ensure the first step in the path is the unit's original position
						if (path[0].x != unit.position.x || path[0].y != unit.position.y) {
							path.unshift(new Point(unit.position.x, unit.position.y));
						}
						if (path.length < 2) {
							callback.call(null, Utils.error(40));
							return;
						} // invalid path
						targetLoc = path[path.length - 1];
					}
					else {
						if (!Board.validPos(game, atts.x, atts.y)) {
							callback.call(null, Utils.error(46));
							return;
						} // cannot position outside of board
						from = new Point(unit.position.x, unit.position.y);
						targetLoc = new Point(atts.x, atts.y);
						dist = Point.distance(unit.position, targetLoc);
						if (dist > unit.properties.mov + 0.5) {
							callback.call(null, Utils.error(16, {
								distance: dist
							}));
							return;
						} // move co-ords out of range
					}
					if (!Board.occupied(game.units, targetLoc)) {
						if (path && path.length > 0) {
							steps = 0;
							for (var i = 1; i < path.length; i++) {
								dist = Point.distance(path[i - 1], path[i]);
								steps += dist;
								if (unit.properties.mov - steps < -1) {
									callback.call(null, Utils.error(16, {
										distance: steps
									}));
									return;
								} // move co-ords out of range
								if (dist > 1.5 || Board.occupied(game.units, path[i])) {
									callback.call(null, Utils.error(40));
									return;
								} // invalid path
								if (!Board.validPos(game, path[i].x, path[i].y)) {
									callback.call(null, Utils.error(46));
									return;
								} // cannot position outside of board
								unit.position = path[i];


								if (Board.getAdjacent(game, unit).length > 0) {
									path = path.slice(0,i);
									unit.moved = true;
									unit.engaged = true;
									actionId = exports.newAction(game, "moved", {
										unit: atts.unit,
										path: path
									});
									exports.newAction(game, "engaged", {
										units: [atts.unit]
									});
									output = {
										success: "Move successful",
										data: {
											moved: {
												unit: atts.unit,
												path: path
											},
											actionId: actionId
										}
									};
									callback.call(null, output);
									return;
								}
							}
							unit.moved = true;
							actionId = exports.newAction(game, "moved", {
								unit: atts.unit,
								path: path
							});
							output = {
								success: "Move successful",
								data: {
									moved: {
										unit: atts.unit,
										path: path
									},
									actionId: actionId
								}
							};
							callback.call(null, output);
							return;
						}
						else {
							unit.position = targetLoc;
							unit.moved = true;
							game.modified = new Date().getTime();
							actionId = exports.newAction(game, "moved", {
								unit: atts.unit,
								from: from,
								to: unit.position
							});

							unit.engaged = false;
							if (Board.getAdjacent(game, unit).length > 0) {
								unit.engaged = true;
								game.modified = new Date().getTime();
								exports.newAction(game, "engaged", {
									units: [atts.unit]
								});
							}
							output = {
								success: "Move successful",
								data: {
									moved: {
										unit: atts.unit,
										from: from,
										to: unit.position
									},
									actionId: actionId
								}
							};
							callback.call(null, output);
							return;
						}
					}
					else {
						dist = Point.distance(unit.position, targetLoc);
						if (path && dist > 1.5) {
							callback.call(null, Utils.error(16, {
								distance: dist
							}));
							return;
						} // move co-ords out of range
						targetUnit = Board.occupied(game.units, targetLoc);
						if (unit == targetUnit) {
							callback.call(null, Utils.error(43));
							return;
						} // cannot attack self
						if (targetUnit.dead) {
							callback.call(null, Utils.error(35));
							return;
						} // unit is dead
						if (unit.properties.com === 0) {
							callback.call(null, Utils.error(33));
							return;
						} // unit cannot attack other units
						if (Unit.hasStatus(targetUnit, "invuln") || targetUnit.properties.def === 0) {
							callback.call(null, Utils.error(32));
							return;
						} // target unit cannot be attacked
						if (Unit.hasStatus(targetUnit, "undead") && (!Unit.hasStatus(unit, "attackUndead") && !Unit.hasStatus(unit, "undead"))) {
							callback.call(null, Utils.error(34));
							return;
						} // unit cannot attack undead unit
						unit.moved = true;
						unit.attacked = true;

						success = false;
						if (Dice.rollAgainst(game, unit.properties.com, targetUnit.properties.def)) {
							success = true;
							actionId = exports.newAction(game, "attacked", {
								unit: unit.id,
								target: targetUnit.id,
								success: true
							});
							exports.killUnit(game, targetUnit);
							if (unit.properties.mov !== 0 && !targetUnit.occupied) {
								unit.position = targetLoc;
								exports.newAction(game, "moved", {
									unit: unit.id,
									from: from,
									to: targetLoc
								});
							}
						}
						else {
							actionId = exports.newAction(game, "attacked", {
								unit: unit.id,
								target: targetUnit.id,
								success: false
							});
						}
						output = {
							success: "Attacked unit",
							data: {
								attacked: {
									unit: unit.id,
									target: targetUnit.id,
									success: success
								},
								actionId: actionId
							}
						};
						callback.call(null, output);
						return;
					}
					break;
				// - attack routine
				case "attacked":
					if (!game.active) {
						callback.call(null, Utils.error(20));
						return;
					} // game has not yet begun
					if (game.ended) {
						callback.call(null, Utils.error(23));
						return;
					} // game has ended
					if (!player) {
						callback.call(null, Utils.error(13));
						return;
					} // player is not in this game
					if (game.players[game.currentPlayer].username !== player.username) {
						callback.call(null, Utils.error(14));
						return;
					} // not this player's turn
					if (!atts.unit) {
						callback.call(null, Utils.error(3));
						return;
					} // unspecified unit name
					if (!game.units[atts.unit]) {
						callback.call(null, Utils.error(6));
						return;
					} // unit does not exist
					unit = game.units[atts.unit];
					if (unit.dead) {
						callback.call(null, Utils.error(35));
						return;
					} // unit is dead
					if (unit.owner !== game.players.indexOf(player)) {
						callback.call(null, Utils.error(19));
						return;
					} // player does not own this unit
					if (unit.attacked === true) {
						callback.call(null, Utils.error(38, {
							unit: atts.unit
						}));
						return;
					} // unit has already attacked
					if (!atts.target) {
						callback.call(null, Utils.error(3));
						return;
					} // unspecified target unit name
					if (!game.units[atts.target]) {
						callback.call(null, Utils.error(6));
						return;
					} // target unit does not exist
					targetUnit = game.units[atts.target];
					if (unit == targetUnit) {
						callback.call(null, Utils.error(43));
						return;
					} // cannot attack self
					if (targetUnit.dead) {
						callback.call(null, Utils.error(35));
						return;
					} // unit is dead
					dist = Point.distance(unit.position, targetUnit.position);
					if (dist > 1.5) {
						callback.call(null, Utils.error(16, {
							distance: dist
						}));
						return;
					} // move co-ords out of range
					from = new Point(unit.position.x, unit.position.y);
					if (unit.properties.com === 0) {
						callback.call(null, Utils.error(33));
						return;
					} // unit cannot attack
					if (Unit.hasStatus(targetUnit, "invuln") || targetUnit.properties.def === 0) {
						callback.call(null, Utils.error(32));
						return;
					} // target unit cannot be attacked
					if (Unit.hasStatus(targetUnit, "undead") && (!Unit.hasStatus(unit, "attackUndead") && !Unit.hasStatus(unit, "undead"))) {
						callback.call(null, Utils.error(34));
						return;
					} // unit cannot attack undead unit
					unit.moved = true;
					unit.attacked = true;
					success = false;
					if (Dice.rollAgainst(game, unit.properties.com, targetUnit.properties.def)) {
						success = true;
						actionId = exports.newAction(game, "attacked", {
							unit: unit.id,
							target: targetUnit.id,
							success: true
						});
						to = {
							x: targetUnit.position.x,
							y: targetUnit.position.y
						};
						exports.killUnit(game, targetUnit);
						if (unit.properties.mov > 0 && !targetUnit.occupied) {
							unit.position = to;
							exports.newAction(game, "moved", {
								unit: unit.id,
								from: from,
								to: to
							});
						}
					}
					else {
						actionId = exports.newAction(game, "attacked", {
							unit: unit.id,
							target: targetUnit.id,
							success: false
						});
					}
					output = {
						success: "Attacked unit",
						data: {
							attacked: {
								unit: unit.id,
								target: targetUnit.id,
								success: success
							},
							actionId: actionId
						}
					};
					callback.call(null, output);
					return;
				// - ranged attack routine
				case "rangedAttacked":
					if (!game.active) {
						callback.call(null, Utils.error(20));
						return;
					} // game has not yet begun
					if (game.ended) {
						callback.call(null, Utils.error(23));
						return;
					} // game has ended
					if (!player) {
						callback.call(null, Utils.error(13));
						return;
					} // player is not in this game
					if (game.players[game.currentPlayer].username !== player.username) {
						callback.call(null, Utils.error(14));
						return;
					} // not this player's turn
					if (!atts.unit) {
						callback.call(null, Utils.error(3));
						return;
					} // unspecified unit name
					if (!game.units[atts.unit]) {
						callback.call(null, Utils.error(6));
						return;
					} // unit does not exist
					unit = game.units[atts.unit];
					if (unit.dead) {
						callback.call(null, Utils.error(35));
						return;
					} // unit is dead
					if (unit.owner !== game.players.indexOf(player)) {
						callback.call(null, Utils.error(19));
						return;
					} // player does not own this unit
					if (unit.properties.rcm === 0) {
						callback.call(null, Utils.error(4));
						return;
					} // unit cannot ranged attack
					if (unit.rangedAttacked === true) {
						callback.call(null, Utils.error(38, {
							unit: atts.unit
						}));
						return;
					} // unit has already attacked
					if (!atts.target) {
						callback.call(null, Utils.error(3));
						return;
					} // unspecified target unit name
					if (!game.units[atts.target]) {
						callback.call(null, Utils.error(6));
						return;
					} // target unit does not exist
					targetUnit = game.units[atts.target];
					if (unit == targetUnit) {
						callback.call(null, Utils.error(43));
						return;
					} // cannot attack self
					if (targetUnit.dead) {
						callback.call(null, Utils.error(35));
						return;
					} // unit is dead
					dist = Point.distance(unit.position, targetUnit.position);
					if (dist > unit.properties.range) {
						callback.call(null, Utils.error(16, {
							distance: dist
						}));
						return;
					} // attack co-ords out of range
					if (Unit.hasStatus(targetUnit, "invuln") || targetUnit.properties.def === 0) {
						callback.call(null, Utils.error(32));
						return;
					} // target unit cannot be attacked
					if (Unit.hasStatus(targetUnit, "undead") && (!Unit.hasStatus(unit, "rangedAttackUndead") && !Unit.hasStatus(unit, "undead"))) {
						callback.call(null, Utils.error(34));
						return;
					} // unit cannot attack undead unit
					unit.moved = true;
					unit.attacked = true;
					unit.rangedAttacked = true;
					success = false;
					if (Dice.rollAgainst(game, unit.properties.rcm, targetUnit.properties.def)) {
						success = true;
						actionId = exports.newAction(game, "rangedAttacked", {
							unit: unit.id,
							target: targetUnit.id,
							success: true
						});
						exports.killUnit(game, targetUnit);
					}
					else {
						actionId = exports.newAction(game, "rangedAttacked", {
							unit: unit.id,
							target: targetUnit.id,
							success: false
						});
					}
					output = {
						success: "Ranged attacked unit",
						data: {
							rangedAttacked: {
								unit: unit.id,
								target: targetUnit.id,
								success: success
							},
							actionId: actionId
						}
					};
					callback.call(null, output);
					return;
				// - create a unit
				case "newUnit":
					if (!game.active) {
						callback.call(null, Utils.error(20));
						return;
					} // game has not yet begun
					if (game.ended) {
						callback.call(null, Utils.error(23));
						return;
					} // game has ended
					if (!player) {
						callback.call(null, Utils.error(13));
						return;
					} // player is not in this game
					if (!atts.type) {
						callback.call(null, Utils.error(3));
						return;
					} // unspecified unit type
					if (!atts.x && !atts.y) {
						callback.call(null, Utils.error(15));
						return;
					} // unspecified move co-ords
					if (!Board.validPos(game, atts.x, atts.y)) {
						callback.call(null, Utils.error(46));
						return;
					} // cannot position outside of board
					targetLoc = new Point(atts.x, atts.y);
					if (Board.occupied(game.units, targetLoc)) {
						callback.call(null, Utils.error(17));
						return;
					} // position already occupied
					unitId = Utils.shortify(game.UID++);
					game.units[unitId] = new Unit(unitId, atts.type, game.players.indexOf(player), new Point(atts.x, atts.y));
					actionId = exports.newAction(game, "newUnit", {
						unit: game.units[unitId]
					});
					output = {
						success: "Created unit",
						data: {
							unit: game.units[unitId],
							actionId: actionId
						}
					};
					callback.call(null, output);
					return;
				// - get the last move made on the game
				case "update":
					if (!game.active) {
						callback.call(null, Utils.error(20));
						return;
					} // game has not started
					if (!atts.since) {
						callback.call(null, {
							success: game.actions
						});
						return;
					} // return all actions
					if (parseInt(atts.since,10) > 418089600) {
						// 'since' is a timestamp
						if (parseInt(atts.since,10) > parseInt(game.actions[game.actions.length - 1].time,10)) {
							callback.call(null, {
								notModified: true
							});
							return;
						}
						var tActions = [];
						for (var ta = 0; a < game.actions.length; ta++) {
							if (atts.since <= game.actions[ta].time) {
								tActions.push(game.actions[ta]);
							}
						}
						if (tActions.length === 0) {
							callback.call(null, {
								notModified: true
							});
							return;
						}
						output = {
							success: tActions,
							by: "timestamp"
						};
						callback.call(null, output);
						return;
					}
					else {
						// 'since' is an action ID
						if (parseInt(atts.since,10) > parseInt(game.actions[game.actions.length - 1].id,10) || parseInt(atts.since,10) < 0) {
							callback.call(null, Utils.error(24));
							return;
						}
						var iActions = [];
						for (var ia = atts.since - 1; ia < game.actions.length; ia++) {
							iActions.push(game.actions[ia]);
						}
						if (iActions.length === 0) {
							callback.call(null, {
								notModified: true
							});
							return;
						}
						output = {
							success: iActions,
							by: "id",
							lastAction: game.actions.length
						};
						callback.call(null, output);
						return;
					}
					break;
				// - refresh the current game
				case "refresh":
					callback.call(null, {
						success: {
							name: game.name,
							created: game.created,
							width: game.width,
							height: game.height,
							players: game.players,
							maxPlayers: game.maxPlayers,
							units: game.units,
							round: game.round,
							currentPlayer: game.currentPlayer,
							lastAction: game.actions.length,
							seed: game.seed,
							ended: game.ended,
							active: game.active,
							modified: game.modified
						}
					});
					return;
				default:
					callback.call(null, Utils.error(21));
					return;
			}
		});
	});
};