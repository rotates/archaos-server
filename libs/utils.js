var winston = require("winston"),
    Hashids = require("hashids"),
    hashids = new Hashids("sv archaos 63109", 5);

var config = {
  levels: {
    silly: 0,
    verbose: 1,
    info: 2,
    data: 3,
    warn: 4,
    debug: 5,
    error: 6
  },
  colors: {
    silly: 'rainbow',
    verbose: 'white',
    info: 'green',
    data: 'magenta',
    warn: 'yellow',
    debug: 'cyan',
    error: 'red'
  }
};

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
		colorize: true,
		level: "verbose"
    }),
    new (winston.transports.File)({
		filename: '././server.log',
		colorize: false,
		maxsize: 1048576,
		json: false
    })
  ],
  levels: config.levels,
  colors: config.colors
});


exports.log = function (line, level) {
	logger.log(level || 'info', line);
};

exports.max = function (a, b) {
	return (a > b) ? a : b;
};

exports.min = function (a, b) {
	return (a < b) ? a : b;
};

exports.find = function (arr, prop, val) {
	for (var e in arr) {
		if (arr[e][prop] == val) {
			return arr[e];
		}
	}
};

exports.numToHash = function (num) {
    return hashids.encrypt(num);
};

exports.hashToNum = function (hash) {
    return hashids.decrypt(hash);
}

exports.shortify = function (num) {
	var list = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-", dec = 0, i;

	num = num.toString();
	for (i = 0; i <= num.length; i++) {
		dec += (list.indexOf(num.charAt(i))) * (Math.pow(10, (num.length - i - 1)));
	}
	num = "";
	var magnitude = Math.floor((Math.log(dec)) / (Math.log(64)));
	for (i = magnitude; i >= 0; i--) {
		var amount = Math.floor(dec / Math.pow(64, i));
		num = num + list.charAt(amount);
		dec -= amount * (Math.pow(64, i));
	}
	return num || "0";
};

exports.error = function (num, extras) {
	var errs = {
		0: "Unspecified error",
		1: "Unspecified game name",
		2: "Unspecified player name",
		3: "Unspecified unit name",
		4: "A game by that name does not exist",
		5: "A player by that name does not exist",
		6: "A unit by that name does not exist",
		7: "A player by that name already exists",
		8: "A game by that name already exists",
		9: "Player is already in this game",
		10: "Game has already begun",
		11: "Game must have two or more players to start",
		12: "Game is full",
		13: "Player is not in this game",
		14: "It is not this player's turn",
		15: "No move co-ordinates specified",
		16: "Out of range",
		17: "Position already occupied",
		18: "Unit has already moved this turn",
		19: "Specified player does not own that unit",
		20: "Game has not started",
		21: "Invalid action",
		22: "Invalid request",
		23: "Game has ended",
		24: "Action number out of range",
		25: "Unspecified password",
		26: "Invalid password",
		27: "Not logged in",
		28: "Already logged in, please log out first",
		29: "Unspecified message",
		30: "String too long",
		31: "Game must be started by game owner",
		32: "Target unit cannot be attacked",
		33: "Unit cannot attack other units",
		34: "Unit cannot attack undead units",
		35: "Dead units cannot attack or be attacked",
		36: "Unit is immobile",
		37: "Unit is engaged",
		38: "Unit has already attacked this turn",
		39: "Unit has already ranged attacked this turn",
		40: "Invalid path",
		41: "Flying units cannot move via a path",
		42: "Non-flying units cannot move via a co-ordinate",
		43: "Units cannot attack themselves",
		44: "Unit cannot ranged attack",
		45: "Missing or invalid access token",
		46: "Cannot position outside of board",
		47: "Error adding player to database",
		48: "Error adding game to database",
		49: "Invalid data request",
        50: "No games found"
	};
	var output = extras || {};
	if (errs[num]) {
		output.error = num;
		output.text = errs[num];
	}
	else {
		output = {
			error: 0,
			text: "Unspecified error"
		};
	}
	return output;
};