var fs = require('fs'),
	Utils = require("./utils.js"),
	units;

var init = function () {
	units = fs.readFileSync("./data/units.json");
	if (units) {
		try {
			Utils.log("Unit data loaded", "data");
			units = JSON.parse(units);
		}
		catch (e) {
			Utils.log("Unit data file could not be found", "error");
			throw new Error("Fatal error: could not find units.json");
		}
	}
}();

var Unit = function (id, type, owner, position) {
	if (!units[type]) {
		Utils.log("Unit of type '" + type + "' could not be found", "error");
		throw new Error("Fatal error: unit type not found");
	}

	this.type = type;
	this.owner = owner;
	this.position = position;
	this.properties = {};
	for (var prop in units[type].properties) {
		this.properties[prop] = units[type].properties[prop];
	}
	this.status = units[type].status.slice(0);
	this.moved = false;
	this.attacked = false;
	this.rangedAttacked = false;
	this.engaged = false;
	this.dead = false;
	this.id = id;
	this.illusion = false;
};

module.exports = Unit;

module.exports.getUnitDefs = function() {
	return units;
};

module.exports.attackableBy = function(unit, target) {
	if (module.exports.hasStatus(target, "invuln")) {
		return false;
	}
	if (module.exports.hasStatus(target, "undead")) {
		if (module.exports.hasStatus(unit, "undead") || module.exports.hasStatus(unit, "attackUndead")) {
			return true;
		}
	}
	return true;
};

module.exports.hasStatus = function (unit, status) {
	if (unit.status.indexOf(status) != -1) {
		return true;
	}
	return false;
};

module.exports.addStatus = function (unit, status) {
	if (!unit.status[status]) {
		unit.status.push(value);
		return true;
	}
	return false;
};

module.exports.removeStatus = function (unit, status) {
	if (unit.status[status]) {
		unit.status.splice(unit.status.indexOf(status), 1);
		return true;
	}
	return false;
};