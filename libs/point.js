var Utils = require("./utils.js");

var Point = function (x, y) {
	x = parseInt(x,10);
	y = parseInt(y,10);
	if (typeof x === "number" && typeof y === "number") {
		this.x = x;
		this.y = y;
	}
};

module.exports = Point;

// thanks to @andy_herbert for this far more authentic distance routine
module.exports.distance = function (pt1, pt2) {
	var diff = {
		x: Math.abs(pt1.x - pt2.x),
		y: Math.abs(pt1.y - pt2.y)
	};
	return Utils.max(diff.x, diff.y) - Utils.min(diff.x, diff.y) + Utils.min(diff.x, diff.y) * 1.5;
};

module.exports.toArray = function () {
	return [x, y];
};