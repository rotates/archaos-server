var Utils = require("./utils.js"),
	Unit = require("./unit.js"),
	Point = require("./point.js");

exports.validPos = function(game, x, y) {
	if (x >= 0 && y >= 0 && x < game.width && y < game.height) {
		return true;
	}
	return false;
};

exports.getAdjacent = function(game, unit, test) {
	var s = [[-1,-1], [0,-1], [1, -1], [-1, 0], [1, 0], [-1,1], [0,1], [1, 1]],
	output = [], u;

	if (!test) {
		test = "engageable";
	}

	for (var i = 0; i < s.length; i++) {
		u = exports.occupied(game.units, {x: unit.position.x+s[i][0], y: unit.position.y+s[i][1]});
		switch (test) {
			case "engageable":
				if (u && u.owner != unit.owner && u.properties.mnv > 0 && u.dead === false) {
					output.push(u);
				}
				break;
			case "enemies":
				if (u && u.owner != unit.owner && u.dead === false) {
					output.push(u);
				}
				break;
			case "friendlies":
				if (u && u.owner == unit.owner && u.dead === false) {
					output.push(u);
				}
				break;
			case "attackable":
				if (u && u.dead === false && Unit.attackableBy(unit, u)) {
					output.push(u);
				}
				break;
			case "live":
				if (u && u.dead === false) {
					output.push(u);
				}
				break;
			case "dead":
				if (u && u.dead === true) {
					output.push(u);
				}
				break;
			case "all":
				if (u) {
					output.push(u);
				}
				break;
		}
	}
	return output;
};

exports.occupied = function (gameUnits, loc) {
	for (var gameUnit in gameUnits) {
		if (gameUnits[gameUnit].position.x == loc.x && gameUnits[gameUnit].position.y == loc.y && !gameUnits[gameUnit].dead) {
			return gameUnits[gameUnit];
		}
	}
	return null;
};

exports.setupWizards = function (game) {
	var gridWidth = game.width,
		gridHeight = game.height;
	var startPositions = {
		"2": [
			[1, Math.floor(gridHeight * 0.5)],
			[gridWidth - 2, Math.floor(gridHeight * 0.5)]
		],
		"3": [
			[Math.floor(gridWidth * 0.5), 1],
			[1, gridHeight - 2],
			[gridWidth - 2, gridHeight - 2]
		],
		"4": [
			[1, 1],
			[gridWidth - 2, 1],
			[1, gridHeight - 2],
			[gridWidth - 2, gridHeight - 2]
		],
		"5": [
			[Math.floor(gridWidth * 0.5), 0],
			[0, Math.floor(gridHeight * 0.4)],
			[gridWidth - 1, Math.floor(gridHeight * 0.4)],
			[Math.floor(gridWidth * 0.2), gridHeight - 1],
			[Math.floor(gridWidth * 0.8), gridHeight - 1]
		],
		"6": [
			[Math.floor(gridWidth * 0.5), 0],
			[0, Math.floor(gridHeight * 0.1)],
			[gridWidth - 1, Math.floor(gridHeight * 0.1)],
			[0, Math.floor(gridHeight * 0.9)],
			[gridWidth - 1, Math.floor(gridHeight * 0.9)],
			[Math.floor(gridWidth * 0.5), gridHeight - 1]
		],
		"7": [
			[Math.floor(gridWidth * 0.5), 0],
			[Math.floor(gridWidth * 0.1), Math.floor(gridHeight * 0.1)],
			[Math.floor(gridWidth * 0.9), Math.floor(gridHeight * 0.1)],
			[0, Math.floor(gridHeight * 0.6)],
			[gridWidth - 1, Math.floor(gridHeight * 0.6)],
			[Math.floor(gridWidth * 0.25), gridHeight - 1],
			[Math.floor(gridWidth * 0.75), gridHeight - 1]
		],
		"8": [
			[0, 0],
			[Math.floor(gridWidth * 0.5), 0],
			[gridWidth - 1, 0],
			[0, Math.floor(gridHeight * 0.5)],
			[gridWidth - 1, Math.floor(gridHeight * 0.5)],
			[0, gridHeight - 1],
			[Math.floor(gridWidth * 0.5), gridHeight - 1],
			[gridWidth - 1, gridHeight - 1]
		]
	};

	var i = 0,
		numPlayers = game.players.length,
		unitId;
	for (var player in game.players) {
		unitId = Utils.shortify(game.UID++);
		game.units[unitId] = new Unit(unitId, "1", game.players.indexOf(game.players[player]), new Point(startPositions[numPlayers][i][0], startPositions[numPlayers][i][1]));
		i++;
	}
};