var crypto = require("crypto"),
	Data = require("./data.js"),
	Utils = require("./utils.js"),
	Game = require("./game.js");

exports.process = function (atts, callback) {
	var player, game, output;
	switch (atts.action) {
		// - ident:
		case "ident":
			if (!atts.username) {
				callback.call(null, Utils.error(2));
				return;
			} // no name specified
			Data.getUser(atts.username, function(player) {
				if (!player) {
					callback.call(null, Utils.error(5));
					return;
				} // player does not exist
				if (player.token === 0) {
					callback.call(null, Utils.error(27));
					return;
				} // not logged in
				if (!atts.token || atts.token != player.token) {
					callback.call(null, Utils.error(45));
					return;
				} // invalid or missing access token
				output = {
					success: "Identified connection"
				};
				callback.call(null, output);
			});
			break;
		// - login
		case "login":
			if (!atts.username) {
				callback.call(null, Utils.error(2));
				return;
			} // no name specified
			if (!atts.password) {
				callback.call(null, Utils.error(25));
				return;
			} // no password specified
			Data.getUser(atts.username, function(player) {
				if (!player) {
					callback.call(null, Utils.error(5));
					return;
				} // player does not exist
				if (player.passwordHash != crypto.createHash('md5').update(atts.password).digest('hex')) {
					callback.call(null, Utils.error(26));
					return;
				} // incorrect password
				var token = crypto.createHash('sha1').update(atts.username + new Date().getTime().toString() + "roflcopter").digest('hex');
				player.token = token;
				player.lastLogin = new Date().getTime();
				Data.save("users", player);
				Utils.log('User \'' + player.username + '\' logged in');
				output = {
					success: "Logged in",
					data: {
						username: player.username,
						handle: player.handle,
						token: token
					}
				};
				callback.call(null, output);
			});
			break;
		// - checkLogin
		case "checkLogin":
			if (!atts.username) {
				callback.call(null, Utils.error(2));
				return;
			} // no name specified
			Data.getUser(atts.username, function(player) {
				if (!player) {
					callback.call(null, Utils.error(5));
					return;
				} // player does not exist
				if (player.token === 0) {
					callback.call(null, Utils.error(27));
					return;
				} // not logged in
				if (!atts.token || atts.token != player.token) {
					callback.call(null, Utils.error(45));
					return;
				} // invalid or missing access token
				output = {
					success: "Logged in",
					data: {
						username: player.username,
						handle: player.handle
					}
				};
				callback.call(null, output);
			});
			break;
		// - logout
		case "logout":
			if (!atts.username) {
				callback.call(null, Utils.error(2));
				return;
			} // no name specified
			Data.getUser(atts.username, function(player) {
				if (!player) {
					callback.call(null, Utils.error(5));
					return;
				} // player does not exist
				if (player.token === 0) {
					callback.call(null, Utils.error(27));
					return;
				} // not logged in
				if (!atts.token || atts.token != player.token) {
					callback.call(null, Utils.error(45));
					return;
				} // invalid or missing access token
				player.token = 0;
				Data.save("users", player);
				Utils.log('User \'' + player.username + '\' logged out');
				output = {
					success: "Logged out",
					data: {
						username: player.username,
						handle: player.handle
					}
				};
				callback.call(null, output);
			});
			break;
		// - create a new player
		case "newplayer":
			if (!atts.username) {
				callback.call(null, Utils.error(2));
				return;
			} // no name specified
			if (atts.username.length > 16) {
				callback.call(null, Utils.error(30, {
					maxLength: 16
				}));
				return;
			} // name too long
			if (!atts.password) {
				callback.call(null, Utils.error(25));
				return;
			} // no password specified
			if (atts.handle) {
				if (atts.handle.length > 32) {
					callback.call(null, Utils.error(30, {
						maxLength: 32
					}));
					return;
				} // handle too long
				handle = atts.handle;
			}
			else {
				handle = atts.username; // no handle specified, so use the player's name
			}
			player = {
				handle: handle,
				username: atts.username,
				id: Utils.numToHash(Data.getUUID()),
				passwordHash: crypto.createHash('md5').update(atts.password).digest('hex'),
				created: new Date().getTime(),
				timesPlayed: 0,
				timesWon: 0,
				timesLost: 0,
				token: 0
			};
			Data.addUser(player, function(data) {
				if (!data) {
					callback.call(null, Utils.error(47)); // database error
					return;
				}
				Utils.log('User \'' + player.username + '\' created');
				output = {
					success: "Created player",
					data: {
						name: player.username,
						handle: player.handle,
						created: player.created
					}
				};
				callback.call(null, output);
			});
			break;
		// - create a new game
		case "newgame":
			if (!atts.username) {
				callback.call(null, Utils.error(2));
			} // no player specified
			Data.getUser(atts.username, function(player) {
				if (!player) {
					callback.call(null, Utils.error(5)); // player does not exist
					return;
				}
				if (player.token === 0) {
					callback.call(null, Utils.error(27));
					return;
				} // not logged in
				if (!atts.token || atts.token != player.token) {
					callback.call(null, Utils.error(45));
					return;
				} // invalid or missing access token

				var gName;
				if (atts.name) {
					if (atts.name.length > 64) {
						callback.call(null, Utils.error(30, {
							maxLength: 64
						}));
					} // name too long
					gName = atts.name;
				}
				else {
					gName = player.handle + "'s Game";
				}

				gCreated = new Date().getTime();

				var maxPlayers = 8;
				if (atts.maxPlayers && atts.maxPlayers > 2 && atts.maxPlayers < 9) {
					maxPlayers = parseInt(atts.maxPlayers,10);
				}

				var width = 15;
				if (atts.width && atts.width > 4 && atts.width < 41) {
					width = parseInt(atts.width,10);
				}

				var height = 15;
				if (atts.height && atts.height > 4 && atts.height < 41) {
					height = parseInt(atts.height,10);
				}

				game = {
				    id: Utils.numToHash(Data.getUUID()),
					name: gName,
					created: gCreated,
					modified: gCreated,
					players: [],
					units: {},
					width: width,
					height: height,
					active: false,
					maxPlayers: maxPlayers,
					UID: 1,
					round: 0,
					phase: 0,
					seed: Math.floor(Math.random() * Math.pow(2, 48)),
					actions: []
				};
				Data.addGame(game, function(data) {
					if (!data) {
						callback.call(null, Utils.error(48)); // database error
						return;
					}
					Game.addPlayer(data, player);
					Game.newAction(data, "newGame", {
						player: {
							username: player.username,
							handle: player.handle
						}
					});
					Utils.log('Game \'' + game.id + '\' created');
					output = {
						success: "Created game",
						data: {
							game: {
								id: game.id,
								name: atts.name,
								created: game.created,
								modified: game.modified,
								by: player.username,
								width: game.width,
								height: game.height,
								maxPlayers: game.maxPlayers
							}
						}
					};
					callback.call(null, output);
				});
			});
			break;
		default:
			callback.call(null, Utils.error(21));
			break;
	}
};