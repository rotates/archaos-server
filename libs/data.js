var fs = require('fs'),
	Utils = require("./utils.js"),
	collections = ["users", "games", "server"],
	db = require("mongojs").connect("archaos", collections);

var properties = {
	connections: {}
}, UUID;

module.exports.properties = properties;

exports.getUUID = function() {
	var uuid = UUID++;
	db.server.update({name: "uuid"}, {$set: {value: uuid}}, {upsert: true});
	return uuid;
};


exports.addUser = function(data, callback) {
	db.users.insert(data, function(err, doc) {
		if (err) {
			callback.call(null, false);
			return;
		}
		callback.call(null, doc[0]);
	});
};

exports.getUser = function(username, callback) {
	db.users.find({username: username}, function(err, users) {
		if (err || !users) {
			callback.call(null, null);
			return;
		}
		callback.call(null, users[0]);
	});
};

exports.addGame = function(data, callback) {
	db.games.insert(data, function(err, doc) {
		if (err) {
			callback.call(null, false);
			return;
		}
		callback.call(null, doc[0]);
	});
};

exports.getGame = function(id, callback) {
	db.games.find({"id": id}, function(err, games) {
		if (err || !games) {
			callback.call(null, null);
			return;
		}
		callback.call(null, games[0]);
	});
};

exports.getGames = function(username, limit, callback) {
    limit = parseInt(limit, 10) || 10;
	db.games.find({"players.username": username}).limit(limit).toArray(function(err, games) {
		if (err || !games || games.length < 1) {
			callback.call(null, Utils.error(50));
			return;
		}
		var output = {success: true, games: []};
		for (var g in games) {
		    output.games.push({
				id: games[g].id,
				modified: games[g].modified,
				name: games[g].name,
				created: games[g].created,
				players: games[g].players,
				active: games[g].active,
				maxPlayers: games[g].maxPlayers,
				round: games[g].round,
				currentPlayer: games[g].currentPlayer
			});
		}
		callback.call(null, output);
	});
};

exports.deleteGame = function(game) {
	db.games.remove({id: game.id}, 1);
};

exports.save = function(collection, doc, callback) {
	db[collection].save(doc, function(err, data) {
		if (data && callback) {
			callback.call(null, data);
		}
	});
};

var init = function () {
	db.server.findOne({name: "uuid"}, function(err, uuid) {
		if (err || !uuid) {
			Utils.log("Server settings could not be found in database, using defaults");
		}
		UUID = uuid.value || 1;
	});
}();